/* Service Worker */
if('serviceWorker' in navigator) { //si le service est disponible sur le navigateur
  navigator.serviceWorker.register('/sw.js').then(function() { //promesse
    console.log('SW inscrit');
  });
}

/* Menu */
$(document).ready( function() {
  let bouton_menu = $('.hamburger');
  let menu = $('header nav');

  bouton_menu.click( function() {
    $(this).toggleClass('is-active');
    menu.toggleClass('is-showed');
  });

  // $(document).click( function(event) {
  //   console.log(event.target);
  //
  //   if (menu.hasClass("is-showed")) {
  //     if ($(event.target).not('.hamburger') && $(event.target).not('.hamburger-box') && $(event.target).not('.hamburger-inner') && $(event.target).not('header nav')){
  //       bouton_menu.toggleClass('is-active');
  //       menu.removeClass("is-showed");
  //       console.log("ok");
  //     } else{
  //       console.log("NOTok");
  //     }
  //   }
  // });
});
