/**************
  BIEN METTRE CE FICHIER A LA RACINE
**************/

let fichiers_offline = [
  '/index.html',
  '/css/style.css',
  '/js/jquery-3.4.1.min.js',
  '/js/app.js'
];

self.addEventListener("install", function(event) {
  console.warn("[SW] version - Install Event"); // Installation du SW
  self.skipWaiting();
  event.waitUntil(
    caches.open("cache-v1").then( function(cache) {
      console.log("Mise en cache des fichiers");
      return cache.addAll(fichiers_offline);
    })
  );
});

self.addEventListener("activate", function(event) {
  console.warn("[SW] version - Activate Event"); // Permet d'activer le SW
});

self.addEventListener("fetch", function(event) {
  console.warn("[SW] version - Fetch Event");
  console.log(event.request);

  // if(event.request.url == "http://localhost:5000/page2.html") {
  //   event.respondWith(
  //     new Response("Requête intercepté et modifiée")
  //   )
  // }

  event.respondWith(
    fromCache(event.request)
      .then(function(response) {
        console.log("Correspondance dans le cache !");

        // mise à jour du cache pour le prochain chargement
        event.waitUntil(
          fetch(event.request).then(function(response) {
             console.log("maj cache pour prochain chargement");
             return updateCache(event.request, response);
          })
        );

        return response;
      })
      .catch(function(echec) {
        console.log("Le fichier n'est pas en cache !");
        return fetch(event.request)
          .then(function(response) {
            return response;
          })
          .catch(function(echec) {
            console.log("Erreur fatale !"+ echec);
          })
      })
  )
});

/* Récupérer les fichiers en cache */
function fromCache(request) {
  return caches.open("cache-v1").then(function(cache) { // on appelle notre cache
    return cache.match(request).then(function(matching) { // on vérifie si le fichier est en cache
      //console.log(matching);
      // si il n'est pas en cache ou si la page n'existe pas, on reject
      if (!matching || matching.status === 404) {
        return Promise.reject("no-match");
      }
      //sinon on retourne la réponse
      else {
        return matching;
      }
    });
  });
}

/* Mettre à jour les fichiers en cache */
function updateCache(request, response) {
  console.log(request);
  console.log(response);
  return caches.open("cache-v1").then(function(cache) {
    return cache.put(request, response);
  });
}
